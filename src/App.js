import React from "react";
import logo from "./logo.svg";
import "./App.css";
import One from "./components/Input";
// import Button from "react-bootstrap/Button";
// import Carousel from "./components/Carousel";
import Layout from "./components/Layout";

import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="App">
      <header className="App-header" style={{ paddingBottom: 20 }}>
        <img src={logo} className="App-logo" alt="logo" />
        {/* <Carousel></Carousel> */}
        <p>Welcome to Unbid's Used Car Emporium!</p>
        <One></One>
        <p>Excited to find out more? Find out below for our inventory list!</p>
        <Layout></Layout>
      </header>
    </div>
  );
}

export default App;
