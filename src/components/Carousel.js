import React from "react";
import Carousel from "react-bootstrap/Carousel";

export default function C() {
  return (
    <Carousel>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/2002-03_Subaru_WRX_sedan.jpg/640px-2002-03_Subaru_WRX_sedan.jpg"
          alt="First slide"
        />
        <Carousel.Caption>
          <h3>Subaru Impreza WRX</h3>
          <p>2002 model with 2.0 L EJ20 H4-T</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/10/Tuned_Mazda_RX-7_Type_RB_%28GF-FD3S%29_front.jpg/640px-Tuned_Mazda_RX-7_Type_RB_%28GF-FD3S%29_front.jpg"
          alt="Second slide"
        />

        <Carousel.Caption>
          <h3>Mazda RX-7</h3>
          <p>1998 model with 1.3 L twin-turbocharged 13B-REW twin-rotor</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Honda_Civic_Type_R_001.JPG/640px-Honda_Civic_Type_R_001.JPG"
          alt="Third slide"
        />

        <Carousel.Caption>
          <h3>Honda Civic Type-R</h3>
          <p>2001 model with 2.0 L K20A I4</p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}
