import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Carlist from "./Carlist";

function Layout() {
  // Dibawah sini
  // Buat array
  const arr = [
    {
      name: "2001 Subaru Impreza WRX Wagon",
      desc: "AWD sports wagon with 2.0 L EJ20 Flat Four",
      image:
        "https://bringatrailer.com/wp-content/uploads/2020/03/2002_subaru_impreza_158498456576b6c5e8f9IMG_1055.jpg?w=940",
      price: 13000,
    },
    {
      name: "1993 Mazda RX-7",
      desc: "RWD sports coupe with 1.3 L 13B-REW Rotary ",
      image:
        "https://bringatrailer.com/wp-content/uploads/2018/09/1993_mazda_rx-7_1537828769ad3ada4dDSC_0005.jpg?w=940",
      price: 35000,
    },
    {
      name: "1993 Honda Civic Si",
      desc: "FWD hot hatchback with 1.6 L D16Z6 Inline 4 with VTEC",
      image:
        "https://bringatrailer.com/wp-content/uploads/2020/05/1993_honda_civic_si_1592436969bc801fb71c976a61.jpg?w=620&resize=620%2C413",
      price: 9000,
    },
    {
      name: "1994 Toyota Supra",
      desc: "RWD sports coupe with 3.0 L 2JZ-GE inline 6",
      image:
        "https://bringatrailer.com/wp-content/uploads/2019/10/1993_toyota_supra_1570922116d208495dDSCN9978-001.jpg?w=940",
      price: 54000,
    },
    {
      name: "1994 Nissan Pathfinder",
      desc: "4WD SUV with 3.0 L VG30E V6",
      image:
        "https://bringatrailer.com/wp-content/uploads/2020/05/1994_nissan_pathfinder_15924297960a6d8330399e24dIMG_1245.jpg?w=940",
      price: 10000,
    },
    {
      name: "1986 Suzuki Samurai JX",
      desc: "4WD mini off-roader with 1.3 L G13BA Inline 4",
      image:
        "https://bringatrailer.com/wp-content/uploads/2019/06/1986_suzuki_samurai_1561244628ee90760eIMG_7661-e1562795001255.jpg?w=940",
      price: 8500,
    },
    {
      name: "1993 Lexus LS400",
      desc: "RWD luxury sedan with 4.0 L 1UZ-FE V8",
      image:
        "https://bringatrailer.com/wp-content/uploads/2019/07/1993_lexus_ls_1563823683208495d56IMG_7495-e1565212849218.jpg?w=940",
      price: 11000,
    },
    {
      name: "1988 Mitsubishi Delica Starwagon Turbodiesel 4WD",
      desc: "4WD van with 2.5 L 4D56 Turbo Diesel Inline 4",
      image:
        "https://bringatrailer.com/wp-content/uploads/2020/04/1988_mitsubishi_delica_15880811190f85ffb1e2746f62IMG_7584.jpg?w=940",
      price: 12000,
    },
  ];
  return (
    <div>
      <Container>
        <Row noGutters>
          {arr.map(function (e) {
            return (
              <Carlist
                name={e.name}
                desc={e.desc}
                image={e.image}
                price={e.price}
              ></Carlist>
            );
          })}
        </Row>
      </Container>
    </div>
  );
}

export default Layout;
