import React from "react";

export default function One() {
  function panggil() {
    console.log("ok, why you click on me?");
  }
  return (
    <div>
      <p onClick={panggil}>
        We offer you quality used cars at very competitive price!
      </p>
    </div>
  );
}
