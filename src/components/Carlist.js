import React from "react";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";

export default function Carlist(props) {
  const formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
    minimumFractionDigits: 2,
  });

  const formattedPrice = formatter.format(props.price);

  // Replace props.name spaces to +
  const q = props.name.replace(/ /g, "+");

  function handleClick() {
    window.open(`https://lmgtfy.com/?q=${q}`, "_blank");
  }

  return (
    <Col xs={12} sm={12} md={6} lg={4} xl={3}>
      <Card text="dark" style={{ width: "20vw", height: "6v5h" }}>
        <Card.Img
          variant="top"
          src={props.image}
          style={{ maxHeight: "175px" }}
        />
        <Card.Body>
          <Card.Title
            style={{
              textAlign: "left",
              height: 70,
            }}
          >
            {props.name}
          </Card.Title>
          <Card.Text
            style={{
              textAlign: "left",
              fontSize: "1rem",
              height: 100,
            }}
          >
            {props.desc}
          </Card.Text>
          <Button variant="primary" onClick={handleClick}>
            {formattedPrice}
          </Button>
        </Card.Body>
      </Card>
    </Col>
  );
}
